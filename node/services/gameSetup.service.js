var redisClient = require("./redisClient");
var roomPlayers = {};
var players = {};
const TEAM = {
  RED: "red",
  BLUE: "blue",
}

module.exports.createRoom = async (gameId,isPrivate) => {
  let roomData = await redisClient.get(gameId); // if (!roomPlayers.hasOwnProperty(gameId)) 
  if (!roomData) {
    roomData = {
      playerCount: 0,
      players: {},
      isPrivate: isPrivate
    }
    redisClient.set(gameId, JSON.stringify(roomData));
    return true;
  }
  else {
    return false;
  }
};

module.exports.adddPlayerToRoom = async (gameId, socketId, playerData) => {
  let roomData = await redisClient.get(gameId);
  if (roomData) {
    roomData = JSON.parse(roomData);
    if (roomData.playerCount >= 4) {
      return false;
    }
    if(isPlayerNameDuplicate(roomData, playerData.screenName)){
      return false;
    }
    roomData.playerCount++;
    let playerObj = this.createPlayerObj(socketId, playerData, roomData.playerCount);
    roomData.players[socketId] = playerObj;
    redisClient.set(gameId, JSON.stringify(roomData));
    return true;
  }
  else {
    return false;
  }
  // roomPlayers[gameId] ={
  //   ...roomPlayers[gameId],
  //   playerIds,
  //   playerCount
  // }
};

module.exports.removePlayerFromRoom = async (gameId, socketId) => {
  let roomPlayers = await redisClient.get(gameId);
  if (roomPlayers) {
    roomPlayers = JSON.parse(roomPlayers);
    let roomData = JSON.parse(JSON.stringify(roomPlayers));
    if (roomData.players.hasOwnProperty(socketId)) {
      delete roomData.players[socketId];
      roomData.playerCount = roomData.playerCount--;
      //reassign host if original host leaves
      roomPlayers = roomData;
      redisClient.set(gameId, JSON.stringify(roomPlayers)); // directly set roomData ?
      return true;
    }
    else {
      return false;
    }
  }
  else {
    return false;
  }
  // delete game room if no more players left and update redis
};

module.exports.getPublicGameToJoin = async () => {
  // get all keys from redis
  const gameIds = await redisClient.keys('*');
  if (gameIds) {
      for(var gameIdIndex in gameIds){
        var gameId = gameIds[gameIdIndex];
        let data = await redisClient.get(gameId);
        if (data) {
          data = JSON.parse(data);
          if (!data.round && !data.isPrivate) {
            console.log("GAME FOUND:", gameId)
            return gameId;
          }
        }
      }
      // gameIds.map(async (gameId) => {
      //   let data = await redisClient.get(gameId);
      //   if (data) {
      //     data = JSON.parse(data);
      //     if (!data.round && !data.isPrivate) {
      //       console.log("GAME FOUND:", gameId)
      //       return gameId;
      //     }
      //   }
      // })
    }
  return false;
}

module.exports.handlePlayerLeft = async (gameId, playerId) =>{
  let result = {}
  let roomData = await redisClient.get(gameId);
  if (roomData) {
    roomData = JSON.parse(roomData);
    // if match is already started just abort the game
    if (roomData.round) {
      // leave game
      result['removePlayer'] = playerId;
      let players = roomData.players;
      delete players[playerId];
      roomData.playerCount--
      if (roomData.playerCount == 0) {
        redisClient.del(gameId);
      }
      else{
        redisClient.set(gameId, JSON.stringify(roomData));
      }
    }
    // still in lobby
    else {
      result['removePlayer'] = playerId;
      let players = roomData.players;
      delete players[playerId];
      roomData.playerCount--;
      if (roomData.playerCount == 0) {
        redisClient.del(gameId);
      }
      else{
        redisClient.set(gameId, JSON.stringify(roomData));
      }
    }

    return result;
  }
}

module.exports.getPlayerDetails = async (gameId, playerId) => {
  let roomData = await redisClient.get(gameId);
  if (roomData) {
    roomData = JSON.parse(roomData);
    if (roomData.players.hasOwnProperty(playerId)) {
      return roomData.players[playerId];
    }
  }
};

module.exports.getOtherRoomPlayers = async (gameId, socketId) => {
  let roomPlayers = await redisClient.get(gameId);
  if (roomPlayers) {
    roomPlayers = JSON.parse(roomPlayers);
    const roomData = JSON.parse(JSON.stringify(roomPlayers)); //does it not already come in stringified form ?
    if (roomData.players.hasOwnProperty(socketId)) {
      delete roomData.players[socketId];
    }
    let otherPlayerData = [];
    Object.keys(roomData.players).map(key => {
      otherPlayerData.push(roomData.players[key]);
    });
    return otherPlayerData;
  }
};

module.exports.createPlayerObj = (socketId, playerData, playerCount) => {
  return {
    avatar: playerData.avatar,
    screenName: playerData.screenName,
    team: playerCount % 2 === 0 ? TEAM.RED : TEAM.BLUE,
    isHost: playerCount === 1 ? true : false,
    id: socketId,
    hasTrump: false,
  }
};

module.exports.changeTeam = async (gameId, socketId, newTeam) => {
  let roomData = await redisClient.get(gameId);
  if (roomData) {
    roomData = JSON.parse(roomData);
    roomData.players[socketId].team = newTeam
    redisClient.set(gameId, JSON.stringify(roomData));
    return true;
  }
  else {
    return false;
  }
};

function isPlayerNameDuplicate(roomData, playerScreenName) {
  const playerIds = Object.keys(roomData.players);

  const result = playerIds.find(player => roomData.players[player].screenName === playerScreenName);
  return result ? true : false;
}


// -- removing object from array at an index i
//         let playerIndex = roomData.playerIds.indexOf(socketId);
//         roomData.playerIds = roomData.playerIds.splice(playerIndex, 1);
//         roomData.playerCount = roomData.playerCount--;
//         roomPlayers[gameId] = roomData;