const redis = require("redis");
const redisClient = redis.createClient();
const util = require('util');
redisClient.get = util.promisify(redisClient.get);
redisClient.keys = util.promisify(redisClient.keys);
redisClient.on("error", function (error) {
  console.error(error);
});

module.exports = redisClient;