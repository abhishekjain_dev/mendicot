const { INITIAL_CARDS, TOTAL_PLAYERS, TEAM_BLUE, TEAM_RED } = require("../utility/constants");
var redisClient = require("./redisClient");
var _ = require('lodash');
const util = require('util')

const singleDeck = require('../utility/Cards'); //[]; // require('../POC/Cards.json')
const { cleanUpProps } = require("../utility/helper");


module.exports.setupNewGameAgain = async (gameId) => {
  let roomData = await redisClient.get(gameId);
  if (roomData) {
    roomData = JSON.parse(roomData);
    let reetDetails = {
      playerCount: roomData.playerCount,
      players: roomData.players,
      isPrivate: roomData.isPrivate
    }
    redisClient.set(gameId, JSON.stringify(reetDetails));
  }
}

module.exports.setupRound = async (gameId) => {
  try {
    let deck = this.shuffleDeck();
    if( !this.isStartingCardsValid(gameId, deck) ) {
      deck = this.shuffleDeck();
    }
    await this.distributeCards(gameId, deck);
    await this.chooseStartingPlayer(gameId);
    await this.setTrumpCardAside(gameId);
    await this.prepareScoreboard(gameId);
    return await this.prepareIndividualPlayerResponse(gameId);
  } catch (error) {
    return false;
  }
  
};

module.exports.shuffleDeck = (noOfDecks = 1) => {
  let deck = [];
  // Setting the number of decks for the game
  while (noOfDecks > 0) {
    let tempDeck = singleDeck.slice(0);
    deck = deck.concat(tempDeck);
    noOfDecks--;
  }

  return this.shuffleArray(deck);
};

module.exports.isStartingCardsValid = async (gameId, deck) => {
  let roomData = await redisClient.get(gameId);
  if (roomData) {
    roomData = JSON.parse(roomData);
    const startingCards = roomData.playerCount === TOTAL_PLAYERS.FOUR_PLAYERS ? INITIAL_CARDS.FOUR_PLAYERS : INITIAL_CARDS.SIX_PLAYERS;
    let offset = 0;
    for(let j=0; j<roomData.playerCount; j++) {
      let cardsUnderTen = 0;
      let noOfAces = 0;
      let noOfMendis = 0;
      for(let i=0; i<startingCards; i++) {
        const card = deck[i+offset];
        if( card.rank < 10 ) cardsUnderTen++;
        if( card.rank === 14 ) noOfAces++;
        if( card.rank === 10 ) noOfMendis++;
      }
      if (cardsUnderTen === startingCards || noOfAces === 4 || noOfMendis === 4) {
        return false;
      }
      offset += startingCards;
    }
    return true;
  }
}

// distribution of cards
module.exports.distributeCards = async (gameId, deck) => {
  let roomData = await redisClient.get(gameId);
  if (roomData) {
    roomData = JSON.parse(roomData);
    const startingCards = roomData.playerCount === TOTAL_PLAYERS.FOUR_PLAYERS ? INITIAL_CARDS.FOUR_PLAYERS : INITIAL_CARDS.SIX_PLAYERS;
    for (let key in roomData.players) {
      roomData.players[key].cardsInHand = deck.splice(0, startingCards);
    }
    redisClient.set(gameId, JSON.stringify(roomData));
  }
  else {
    throw new Error('Gameplay Server Error');
  }
};

// randomly choose starting player
// add round and turns json in the main json
module.exports.chooseStartingPlayer = async (gameId, roundNo = 1) => {
  let roomData = await redisClient.get(gameId);
  if (roomData) {
    roomData = JSON.parse(roomData);
    let playerArr = Object.keys(roomData.players);
    let startIndex = Math.floor(Math.random() * roomData.playerCount);
    // make playerArr such that alternating team colour
    playerArr = this.sortPlayerByTeams(playerArr, roomData.players).slice(0);
    let turns = [];
    for (let i=0; i<4; i++) {
      if (startIndex > 3) startIndex = 0;
      turns.push(playerArr[startIndex]);
      startIndex++;
    }
    // playerArr = this.shuffleArray(playerArr).slice(0);
    let round = {
      turns,
      roundNo,
      nextTurn: turns[1],
      currentTurn: turns[0],
      cardsPlayed: [],
      // commonData: {},
    }
    roomData.round = round;
    roomData.commonData = {};
    redisClient.set(gameId, JSON.stringify(roomData));
  }
}

module.exports.sortPlayerByTeams = (playerArr, playersObj) => {
  let redPlayerIndex = 0;
  for (let i=0; i<playerArr.length; i++) {
    if (playersObj[playerArr[i]].team === TEAM_RED) {
      if (i !== redPlayerIndex) {
        let redPlayer = playerArr.splice(i, 1);
        playerArr.splice(redPlayerIndex, 0, redPlayer[0]);
      }
      redPlayerIndex += 2;
    }
  }
  return playerArr;
}

module.exports.setTrumpCardAside = async (gameId) => {
  let roomData = await redisClient.get(gameId);
  if (roomData) {
    roomData = JSON.parse(roomData);
    const startingCards = roomData.playerCount === TOTAL_PLAYERS.FOUR_PLAYERS ? INITIAL_CARDS.FOUR_PLAYERS : INITIAL_CARDS.SIX_PLAYERS;
    const randomIndex = Math.floor(Math.random() * startingCards);
    const trumpHolder = roomData.round.turns[roomData.playerCount - 1];  // socket id of the trump holder, last person in the turns array
    let trumpCard = roomData.players[trumpHolder].cardsInHand.splice(randomIndex, 1);

    roomData.players[trumpHolder].hasTrump = true;
    roomData.commonData['trumpHolder'] = trumpHolder;

    roomData = {
      ...roomData,
      commonData: {
        ...roomData.commonData,
        isTrumpRevealed: false,
        trumpCard: trumpCard[0],
      },
    }
    redisClient.set(gameId, JSON.stringify(roomData));
  }
}

module.exports.prepareIndividualPlayerResponse = async (gameId) => {
  let roomData = await redisClient.get(gameId);
  if (roomData) {
    roomData = JSON.parse(roomData);
    const { playerCount, commonData, round, players } = roomData;
    cleanUpProps(commonData, ['trumpCard']);
    let playerResponses = {};
    for (let key in players) {
      let playerObj = {
        playerCount,
        commonData,
        round,
        playerData: players[key],
      };

      playerResponses[key] = playerObj;
    }
    return playerResponses;
  }
}


module.exports.prepareScoreboard = async (gameId) => {
  let roomData = await redisClient.get(gameId)
  if(roomData){
    roomData = JSON.parse(roomData);
    let scoreBoard = {
      gameNo: 1,
      red: {
        roundsWon: 0,
        mendi: []
      },
      blue: {
        roundsWon: 0,
        mendi: []
      },
    }
    roomData['scoreBoard'] = scoreBoard;
    redisClient.set(gameId, JSON.stringify(roomData))
  }
}


module.exports.trumpOpened = async (gameId, by) => {
  let result = {}
  let roomDataString = await redisClient.get(gameId);
  if (roomDataString) {
    let roomData = JSON.parse(roomDataString);

    // check if card is not already opened
    if (!roomData.commonData.isTrumpRevealed) {
      playerCards = roomData.players[by].cardsInHand;
      let roundDetails = roomData.round
      if (roundDetails.cardsPlayed.length) {
        let firstCardPlayed = roundDetails.cardsPlayed[0].card;
        let canTrumpBeOpened = true;

        // check if the player can open trump
        playerCards.some(card => {
          if (card.type === firstCardPlayed.type && !card.isPlayed) {
            canTrumpBeOpened = false;
            return true;
          }
        });
        if (canTrumpBeOpened) {
          roomData.commonData.isTrumpRevealed = true;
          roomData.players[roomData.commonData.trumpHolder].cardsInHand.push(roomData.commonData.trumpCard);
          roomData.round['trumpOpenedRound'] = { roundNo: roomData.round.roundNo, player: by};
          redisClient.set(gameId, JSON.stringify(roomData));
          result['trump'] = roomData.commonData;
        }
        else {
          console.log("player cant open trump")
          result['error'] = "Cannot open Trump yet";
        }
      }
      else {
        //  if the round is not started then player can only open trump if he is holding the trump and he has no other cards in hand

        // check if player holds the turmp
        if (by == roomData.commonData.trumpHolder){

          // check if player has played all other cards
          let areAllCardsPlayed = true;
          playerCards.some(card => {
            if (!card.isPlayed) {
              areAllCardsPlayed = false;
              return true;
            }
          });

          // if all other cards are played then player can open the trump
          if(areAllCardsPlayed){
            roomData.commonData.isTrumpRevealed = true;
            roomData.players[roomData.commonData.trumpHolder].cardsInHand.push(roomData.commonData.trumpCard);
            roomData.round['trumpOpenedRound'] = { roundNo: roomData.round.roundNo, player: by };
            redisClient.set(gameId, JSON.stringify(roomData));
            result['trump'] = roomData.commonData;
          }
          else{
            result['error'] = "You can still play other cards";
          }
        }
        else{
          result['error'] = "Cannot open trump First";
        }
      }
    }
    else{
      result['error'] = "Trump already Opened";
    }
    return result;
  }
  else {
    result['error'] = "Game not Found";
  }
}


// choosing next round player;
// each round winner logic

module.exports.cardPlayed = async (gameId, by, card) => {
  var finalresult = {}
  let roomDataString = await redisClient.get(gameId);
  if (roomDataString) {
    let roomData = JSON.parse(roomDataString);
    if (roomData.gameOver){
      return;
    }
    
    // get the card played by the player
    // verify if the card played is eligible and played by correct player
    let round = roomData.round;
    let playyy = roomData['players'];
    if (round.currentTurn === by) {
      let playerCards = roomData['players'][by].cardsInHand;
      if (this.properCardPlayed(round.cardsPlayed, playerCards, card)) {

        // change the player card isPlayed to true
        let cardIndex = _.findIndex(playerCards, card)
        let pCard = playerCards[cardIndex];
        pCard.isPlayed = true;
        playerCards[cardIndex] = pCard;

        //add the card in the round json
        round.cardsPlayed.push({ card: pCard, player: by });

        // check if round is over
        if (round.cardsPlayed.length === roomData.playerCount) {

          // await delay(1000);
          console.log("round over");

          // check winner
          let winner = this.verifyWinner(round, roomData.commonData.isTrumpRevealed, roomData.commonData.trumpCard);

          // add winner to scoreboard
          let scoreBoard = roomData['scoreBoard']
          let winningPlayerTeam = roomData['players'][winner.winner].team;
          let teamScoreboard = scoreBoard[winningPlayerTeam];
          teamScoreboard.roundsWon++;
          if (winner.hasMendi) {
            teamScoreboard.mendi = teamScoreboard.mendi.concat(winner.mendi);
          }

          finalresult['scoreBoard'] = scoreBoard;

          // checck if game is over
          if ((scoreBoard[TEAM_BLUE].mendi.length + scoreBoard[TEAM_RED].mendi.length) == 4) {
            // GAME OVER
            if (scoreBoard[TEAM_BLUE].mendi.length == scoreBoard[TEAM_RED].mendi.length){

              // game over will depend on round roundsWon
              if ((scoreBoard[TEAM_BLUE].roundsWon + scoreBoard[TEAM_RED].roundsWon) == 12){
                console.log("GAME OVER!!");
                let winnindTeam = ''
                if (scoreBoard[TEAM_BLUE].roundsWon > scoreBoard[TEAM_RED].roundsWon) {
                  winnindTeam = TEAM_BLUE;
                }
                else {
                  winnindTeam = TEAM_RED
                }
                finalresult['gameOver'] = {
                  winnindTeam: winnindTeam
                };
                roomData['gameOver'] = true;
              }
            }
            else{
              let winnindTeam = ''
              if (scoreBoard[TEAM_BLUE].mendi.length > scoreBoard[TEAM_RED].mendi.length){
                winnindTeam = TEAM_BLUE;
              }
              else{
                winnindTeam = TEAM_RED
              }
              console.log("GAME OVER!!");
              finalresult['gameOver'] = {
                winnindTeam: winnindTeam
              };
              roomData['gameOver'] = true;

            }

          }

          // reset rounds
          round.currentTurn = winner.winner;
          round.roundNo += 1
          let nextPlayerIndex = _.indexOf(round.turns, round.currentTurn) + 1
          round.nextTurn = nextPlayerIndex >= roomData.playerCount ? round.turns[0] : round.turns[nextPlayerIndex]
          finalresult['round'] = JSON.parse(JSON.stringify(roomData.round));
          round.cardsPlayed = [];


        }
        else {
          console.log('Round not ended changing round turns')
          //change nextTurn currentTurn
          round.currentTurn = round.nextTurn;
          let nextPlayerIndex = _.indexOf(round.turns, round.currentTurn) + 1
          round.nextTurn = nextPlayerIndex >= roomData.playerCount ? round.turns[0] : round.turns[nextPlayerIndex]
          finalresult['round'] = roomData.round;

        }


      }
      else {
        // something wrong
        console.log("🚀 ~ file: gameplayService.js ~ line 181 ~ module.exports.cardPlayed= ~ not peoper player", "not peoper player")
        finalresult['error'] = "You cant play this card right now";
        

      }
    }
    else {
      // not the players turn
      console.log("🚀 ~ file: gameplayService.js ~ line 181 ~ module.exports.cardPlayed= ~ not peoper player", "not players turn")
      finalresult['error'] = "Its not your turn to play";

    }
    redisClient.set(gameId, JSON.stringify(roomData));
    if(!finalresult['error']){
      delete finalresult.round.turns
    }
    return finalresult

    //verify the round if complete or not
  }

}

module.exports.properCardPlayed = (roundCards, playerCards, cardPlayed) => {
  let result = false;

  // check if player has that card
  if (_.findIndex(playerCards, cardPlayed) < 0) {
    result = false;
    return result;
  }

  // Check if first card is played for that round
  if (roundCards.length) {
    let firstCardPlayed = roundCards[0].card;
    //check if type is same 
    if (cardPlayed.type === firstCardPlayed.type && !cardPlayed.isPlayed) {
      // all good
      result = true;
    }
    // if type is not same then the player should not have that card type
    else {
      if (this.doesPlayerHaveValidCardInHand(playerCards, firstCardPlayed.type)) {
        result = false;
      }
      else {
        result = true;
      }
    }
  }
  else {
    // nothing to check as it is the first card
    result = true;
  }
  return result;

}

async function delay(number) {
  return new Promise(resolve => setTimeout(resolve, number));
}

module.exports.verifyWinner = (round, isTrumpOpen, trump) => {
  let allCards = round.cardsPlayed
  let currentRound = round.roundNo;
  let trumpOpenedRound = 0;
  let trumpPlayer = '';
  if (round.trumpOpenedRound){
    trumpOpenedRound = round.trumpOpenedRound.roundNo;
    trumpPlayer = round.trumpOpenedRound.player;
  }
  let result = {
    hasMendi: false,
    winner: "",
    mendi: []
  }
  let initialCard = allCards[0];
  let initialType = initialCard.card.type;
  let highestValuedCard = initialCard;
  let mendiCard;
  allCards.map(holder => {

    //match among the card types
    let card = holder.card
    if (card.type === highestValuedCard.card.type) {
      if (card.rank > highestValuedCard.card.rank) {
        highestValuedCard = holder;
      }
    }
    if (isTrumpOpen) {
      // check if card type is same as trump type

      // *************** trump bug fix logic *******************
      // whoever opens trump and is the same person plays the trump he is always the highest
      if(currentRound == trumpOpenedRound && holder.player == trumpPlayer && card.type == trump.type){
        highestValuedCard = holder;
      }


      else if (card.type == trump.type) {
        // check if highest valued card is already trump
        if (highestValuedCard.card.type == trump.type) {
          if (card.rank > highestValuedCard.card.rank) {
            highestValuedCard = holder;
          }
        }
        // else current card becomes highest valued coz its a trump card
        else {
          highestValuedCard = holder;
        }
      }
    }

    // check if there is a mendi
    if (card.rank == 10) {
      result.hasMendi = true;
      mendiCard = card;
      result.mendi.push(mendiCard);
    }

  })

  result.winner = highestValuedCard.player;

  return result;


}

module.exports.turnTimedOut = async (gameId) => {
  let roomData = await redisClient.get(gameId);
  try {
    if (roomData) {
      roomData = JSON.parse(roomData);
      let currentTurnId = roomData.round.currentTurn;
      let playerCards = roomData.players[currentTurnId].cardsInHand;
      let cardToBePlayed = null;
      let currentCardType = null;
      if (roomData.round.cardsPlayed.length && roomData.round.cardsPlayed[0].card.type){
        currentCardType = roomData.round.cardsPlayed[0].card.type
      }
      cardToBePlayed = this.getValidCardFromPlayersHand(playerCards, currentCardType);
      // if (!cardToBePlayed) {
      //   cardToBePlayed = playerCards[0];
      // }

      if (cardToBePlayed){
        return await this.cardPlayed(gameId, currentTurnId, cardToBePlayed);
      }
      else{
        return false;
      }
    }
    else {
      return false
    }
  } catch (error) {
    console.log('Error in turnTimeout= ', error)
    return false;
  }
}

module.exports.shuffleArray = (arr) => {
  //In PLACE SHUFFLING
  let t;
  let n = arr.length;
  while (n) {
    let i = Math.floor(Math.random() * n--);
    t = arr[n];
    arr[n] = arr[i];
    arr[i] = t;
  }
  return arr;
}

module.exports.doesPlayerHaveValidCardInHand = (playerCards, cardType) => {
  return playerCards.some(card => {
    if (card.type === cardType && !card.isPlayed) {
      return true;
    }
  });
}

module.exports.getValidCardFromPlayersHand = (playerCards, cardType) => {
  let card =  playerCards.find(card => {
    if (card.type === cardType && !card.isPlayed) {
      return card;
    }
  });

  // give any card coz player does not have the card with that card type
  if(!card){
    card = playerCards.find(card => {
      if (!card.isPlayed) {
        return card;
      }
    });

    // if all cards are played then the player has to pen trump
    if(!card){
      // not handeling this condition right now coz its tricky
      return false;
    }
  }
  return card
}

// Algorithm for shuffling deck
/*
// COMPACT SHUFFLING
let shuffledDeck = [];
let n = deck.length;
while(n) {
  let i = Math.floor(Math.random() * n--);
  shuffledDeck.push(decck.splice(i, 1));
}
*/