const socket = require('socket.io');
const _ = require('lodash');
const util = require('util');

const gameSetupService = require('./gameSetup.service');
const gameplayService = require('./gameplayService');

const { getRandomString } = require('../utility/helper');
const { constructSocketResponse } = require('../utility/responseGeneration');

const GAME_NOT_FOUND_ERROR = {
  status: 404,
  message: 'Game not Found'
}

var io = null;

const socketService = {

  initialiseSocket(server) {
    io = socket(server, {
      cors: {
        origin: "*",
        methods: ["GET,HEAD,PUT,PATCH,POST,DELETE"]
      }
    });
    var clients = 0;
    io.on('connection', (socketInstance) => {
      clients++
      console.log('made socket connection: ' + socketInstance.id)

      io.sockets.emit('clientCount', clients)

      socketInstance.on('disconnect', function (data) {
        console.log('disconnected client ' + socketInstance.id)
        clients--;
        io.sockets.emit('clientCount', clients)
      })


      // GAME SETUP RELATED SERVICES
      socketInstance.on('newGame', async function (data) {
        const gameId = getRandomString(5);
        // const gameId = '123'; // UUID
        if (await gameSetupService.createRoom(gameId,data.isPrivate)) {
          if (await gameSetupService.adddPlayerToRoom(gameId, socketInstance.id, data)) {
            socketInstance.join(gameId);
            const playerDetails = await gameSetupService.getPlayerDetails(gameId, socketInstance.id)
            playerDetails['gameId'] = gameId;
            socketInstance.emit('gameCreated', playerDetails);
          }
          else {
            socketInstance.emit('error', constructSocketResponse(60000, 'Could not add player to Room'));
          }
        }
        else {
          socketInstance.emit('error', constructSocketResponse(60000, 'Could not create Room'));
        }
      });

      socketInstance.on('joinGame', async function (data) {
        if (await gameSetupService.adddPlayerToRoom(data.gameId, socketInstance.id, data)) {
          socketInstance.join(data.gameId);
          const playerDetails = await gameSetupService.getPlayerDetails(data.gameId, socketInstance.id);
          const otherRoomPlayers = await gameSetupService.getOtherRoomPlayers(data.gameId, socketInstance.id);
          const response = {
            gameId: data.gameId,
            playerDetails,
            otherRoomPlayers,
          };
          socketInstance.to(data.gameId).emit('newPlayerJoined', playerDetails);
          socketInstance.emit('gameJoined', response);
        }
        else {
          socketInstance.emit('error', GAME_NOT_FOUND_ERROR);
        }
      });

      socketInstance.on('joinRandomGame', async function(){
        const key = await gameSetupService.getPublicGameToJoin();
        socketInstance.emit('randomGameKey', key);
      });

      // Still need to handle assigning host and correcting roomData
      socketInstance.on('leaveGame', async function (data) {
        if (await gameSetupService.removePlayerFromRoom(data.gameId, socketInstance.id)) {
          socketInstance.to(data.gameId).emit('PlayerLeft', socketInstance.id);
        }
        else {
          socketInstance.emit('error', constructSocketResponse(60000, 'Could not find Room'));
        }
      });

      socketInstance.on('changeTeam', async function (data) {
        if (await gameSetupService.changeTeam(data.gameId, data.id, data.newTeam)) {
          // socketInstance.to(data.gameId).emit('changeTeam', data);
          io.to(data.gameId).emit('changeTeam', data)
        }
        else {
          socketInstance.emit('error', constructSocketResponse(60000, 'Could not find Room'));
        }
      });


      // ROUND RELATED SERVICES
      socketInstance.on('startNewGameAgain', async (data)=>{  
        await gameplayService.setupNewGameAgain(data.gameId);
        io.to(data.gameId).emit('restartGame')
      })


      socketInstance.on('startGame', async (data) => {
        const playerResponses = await gameplayService.setupRound(data.gameId);
        if (playerResponses) {
          for (let key in playerResponses) {
            io.to(key).emit('roundStart', playerResponses[key]);
          }
        }
        else {
          socketInstance.emit('error', constructSocketResponse(60000, 'Gameplay Server Error'));
        }
        // io.in(data.gameId).emit('scoreBoard', result.scoreBoard)
      });

      socketInstance.on('cardPlayed', async (data) =>{
        let roomPlayersInfo = await gameplayService.cardPlayed(data.gameId, socketInstance.id, data.card);
        let result = JSON.parse(JSON.stringify(roomPlayersInfo));
        if(result['round']){
          io.in(data.gameId).emit('cardPlayed', result.round)
        }
        if(result['error']){
          socketInstance.emit('error', constructSocketResponse(60000, result.error));
        }
        if (result['scoreBoard']){
          // scoreboard changed
          await delay(5000);
          io.in(data.gameId).emit('scoreBoard', result['scoreBoard'])
        }
        if(result['gameOver']){
          io.in(data.gameId).emit('gameOver', result['gameOver'])
        }
      });

      socketInstance.on('turnTimedOut', async (data) =>{
        let cardPlayedResult = await gameplayService.turnTimedOut(data.gameId);
        if(!cardPlayedResult){
          return
        }
        // Make below code into separate function
        let result = JSON.parse(JSON.stringify(cardPlayedResult));
        if(result['round']){
          io.in(data.gameId).emit('cardPlayed', result.round)
        }
        if(result['error']){
          socketInstance.emit('error', constructSocketResponse(60000, result.error));
        }
        if (result['scoreBoard']){
          // scoreboard changed
          await delay(5000);
          io.in(data.gameId).emit('scoreBoard', result['scoreBoard'])
        }
        if(result['gameOver']){
          io.in(data.gameId).emit('gameOver', result['gameOver'])
        }
      });

      async function delay(number) {
        return new Promise(resolve => setTimeout(resolve, number));
      }

      // trydis
      socketInstance.on('playerDisconnected', async (data) => {
        const result = await gameSetupService.handlePlayerLeft(data.gameId, socketInstance.id);
        if (result) {
          io.in(data.gameId).emit('playerDisconnected', result);
        }
      })
      socketInstance.on('trumpOpened', async (data) => {
        const result = await gameplayService.trumpOpened(data.gameId, socketInstance.id);
        if (result.error) {
          socketInstance.emit('error', constructSocketResponse(60000, result.error));
        }
        else{
          io.in(data.gameId).emit('trumpOpened', result.trump)
          socketInstance.emit('trumpOpenedByMy');
        }
      })


      //CHAT RELATED SERVICES

      socketInstance.on('typing', function (data) {
        socketInstance.broadcast.emit('typing', data);
      });
      socketInstance.on('chat', function (data) {
        socketInstance.emit('chat', data);
      });
    })
    return io;
  },

}

module.exports = socketService;



// io.emit()                            - goes to all connections
// socket.emit()                        - goes back to the FE socket who made the emit
// socket.broadcast.emit()              - goes to all the other connections except the caller
// socket.join(roomName)                - Join the room specified
// socket.to(roomName).emit()           - To everyone in that room except the sender
// socket.to(roomName).broadcast.emit() - to everyone in room except caller
// io.in(roomName).emit();              - to all clients in roomName

// https://socket.io/docs/v3/emit-cheatsheet/index.html