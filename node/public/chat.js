var socketFeInstance = io.connect('http://localhost:8000');

var socket = io('/my-namespace');

// Query DOM
var message = document.getElementById('message'),
      handle = document.getElementById('handle'),
      btn = document.getElementById('send'),
      output = document.getElementById('output');
      feedback = document.getElementById('feedback');


// Emitting events

btn.addEventListener('click', function(){
  socketFeInstance.emit('chat', {
    message: message.value,
    handle: handle.value
  })
})

message.addEventListener('keypress', function(){
  socketFeInstance.emit('typing', handle.value);
})

// Listen for events
socketFeInstance.on('chat', function(data){
  output.innerHTML += '<p><strong>'+ data.handle + '</strong>: ' +data.message+ '</p>';
  feedback.innerHTML = '';
})

socketFeInstance.on('typing', function(data){
  feedback.innerHTML = '<p><em>'+data+ ' is typing a message...</em></p>'
})

socketFeInstance.on('clientCount', function(data) {
  output.innerHTML += '<p>'+data + ' Clients in session</p>'
})