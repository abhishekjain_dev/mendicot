const router = require('express').Router();
const socketService = require('../services/sockets.service');

router.get('/', (req, res)=> {
  res.json('GameController');
});

router.post('/', (req, res)=>{
  const gameId = Math.floor(Math.random() * 1000) // replace with UUID
  var game = io.of(`/${gameId}`);

  game.on('connection', (socketInst) => {
    console.log('Connected to the namespace '+ gameId)
  });

  res.json({
    gameId: gameId
  })
});

module.exports = router;