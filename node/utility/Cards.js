module.exports = [
  {
    "imgName": "AH",
    "isPlayed": false,
    "type": "Hearts",
    "rank": 14
  },
  {
    "imgName": "3H",
    "isPlayed": false,
    "type": "Hearts",
    "rank": 3
  },
  {
    "imgName": "4H",
    "isPlayed": false,
    "type": "Hearts",
    "rank": 4
  },
  {
    "imgName": "5H",
    "isPlayed": false,
    "type": "Hearts",
    "rank": 5
  },
  {
    "imgName": "6H",
    "isPlayed": false,
    "type": "Hearts",
    "rank": 6
  },
  {
    "imgName": "7H",
    "isPlayed": false,
    "type": "Hearts",
    "rank": 7
  },
  {
    "imgName": "8H",
    "isPlayed": false,
    "type": "Hearts",
    "rank": 8
  },
  {
    "imgName": "9H",
    "isPlayed": false,
    "type": "Hearts",
    "rank": 9
  },
  {
    "imgName": "10H",
    "isPlayed": false,
    "type": "Hearts",
    "rank": 10
  },
  {
    "imgName": "JH",
    "isPlayed": false,
    "type": "Hearts",
    "rank": 11
  },
  {
    "imgName": "QH",
    "isPlayed": false,
    "type": "Hearts",
    "rank": 12
  },
  {
    "imgName": "KH",
    "isPlayed": false,
    "type": "Hearts",
    "rank": 13
  },
  {
    "imgName": "AC",
    "isPlayed": false,
    "type": "Clubs",
    "rank": 14
  },
  {
    "imgName": "3C",
    "isPlayed": false,
    "type": "Clubs",
    "rank": 3
  },
  {
    "imgName": "4C",
    "isPlayed": false,
    "type": "Clubs",
    "rank": 4
  },
  {
    "imgName": "5C",
    "isPlayed": false,
    "type": "Clubs",
    "rank": 5
  },
  {
    "imgName": "6C",
    "isPlayed": false,
    "type": "Clubs",
    "rank": 6
  },
  {
    "imgName": "7C",
    "isPlayed": false,
    "type": "Clubs",
    "rank": 7
  },
  {
    "imgName": "8C",
    "isPlayed": false,
    "type": "Clubs",
    "rank": 8
  },
  {
    "imgName": "9C",
    "isPlayed": false,
    "type": "Clubs",
    "rank": 9
  },
  {
    "imgName": "10C",
    "isPlayed": false,
    "type": "Clubs",
    "rank": 10
  },
  {
    "imgName": "JC",
    "isPlayed": false,
    "type": "Clubs",
    "rank": 11
  },
  {
    "imgName": "QC",
    "isPlayed": false,
    "type": "Clubs",
    "rank": 12
  },
  {
    "imgName": "KC",
    "isPlayed": false,
    "type": "Clubs",
    "rank": 13
  },
  {
    "imgName": "AD",
    "isPlayed": false,
    "type": "Diamonds",
    "rank": 14
  },
  {
    "imgName": "3D",
    "isPlayed": false,
    "type": "Diamonds",
    "rank": 3
  },
  {
    "imgName": "4D",
    "isPlayed": false,
    "type": "Diamonds",
    "rank": 4
  },
  {
    "imgName": "5D",
    "isPlayed": false,
    "type": "Diamonds",
    "rank": 5
  },
  {
    "imgName": "6D",
    "isPlayed": false,
    "type": "Diamonds",
    "rank": 6
  },
  {
    "imgName": "7D",
    "isPlayed": false,
    "type": "Diamonds",
    "rank": 7
  },
  {
    "imgName": "8D",
    "isPlayed": false,
    "type": "Diamonds",
    "rank": 8
  },
  {
    "imgName": "9D",
    "isPlayed": false,
    "type": "Diamonds",
    "rank": 9
  },
  {
    "imgName": "10D",
    "isPlayed": false,
    "type": "Diamonds",
    "rank": 10
  },
  {
    "imgName": "JD",
    "isPlayed": false,
    "type": "Diamonds",
    "rank": 11
  },
  {
    "imgName": "QD",
    "isPlayed": false,
    "type": "Diamonds",
    "rank": 12
  },
  {
    "imgName": "KD",
    "isPlayed": false,
    "type": "Diamonds",
    "rank": 13
  },
  {
    "imgName": "AS",
    "isPlayed": false,
    "type": "Spades",
    "rank": 14
  },
  {
    "imgName": "3S",
    "isPlayed": false,
    "type": "Spades",
    "rank": 3
  },
  {
    "imgName": "4S",
    "isPlayed": false,
    "type": "Spades",
    "rank": 4
  },
  {
    "imgName": "5S",
    "isPlayed": false,
    "type": "Spades",
    "rank": 5
  },
  {
    "imgName": "6S",
    "isPlayed": false,
    "type": "Spades",
    "rank": 6
  },
  {
    "imgName": "7S",
    "isPlayed": false,
    "type": "Spades",
    "rank": 7
  },
  {
    "imgName": "8S",
    "isPlayed": false,
    "type": "Spades",
    "rank": 8
  },
  {
    "imgName": "9S",
    "isPlayed": false,
    "type": "Spades",
    "rank": 9
  },
  {
    "imgName": "10S",
    "isPlayed": false,
    "type": "Spades",
    "rank": 10
  },
  {
    "imgName": "JS",
    "isPlayed": false,
    "type": "Spades",
    "rank": 11
  },
  {
    "imgName": "QS",
    "isPlayed": false,
    "type": "Spades",
    "rank": 12
  },
  {
    "imgName": "KS",
    "isPlayed": false,
    "type": "Spades",
    "rank": 13
  }
]