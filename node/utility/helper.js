module.exports = {

  cleanUpProps(jsonObj, propsToClean) {
    propsToClean.map(prop => {
      if(jsonObj.hasOwnProperty(prop)) {
        delete jsonObj[prop];
      }
    });
  },

  getRandomString(length) {
    // const referenceStr = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
    const referenceStr = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
    const referenceStrLength = referenceStr.length;
    let randomStr = '';
    while(length > 0){
      randomStr += referenceStr.charAt(Math.floor(Math.random()*referenceStrLength));
      length--;
    }
    return randomStr;
  },

  // Inclusive of Min, exclusive of Max
  getRandomIntegerBetween(min, max) {
    return Math.floor(Math.random() * (max-min)) + min;
  },

}