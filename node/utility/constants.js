module.exports = {

  INITIAL_CARDS: {
    FOUR_PLAYERS: 12,
    SIX_PLAYERS: 8,
  },

  TOTAL_PLAYERS: {
    FOUR_PLAYERS: 4,
    SIX_PLAYERS: 6,
  },

  TEAM_RED: 'red',

  TEAM_BLUE: 'blue',

  RESPONSE_CODE: {
    SUCCESS: 200,
    BAD_PARAMS: 400,
    NOT_FOUND: 404,
    GENERAL_ERROR: 50000,
    SOCKET_ERROR: 60000,
    REDIS_ERROR: 70000,
  },

  RESPONSE_TYPE: {
    SUCCESS: 'SUCCESS',
    BAD_PARAMS: 'BAD_PARAMS',
    NOT_FOUND: 'NOT_FOUND',
    GENERAL_ERROR: 'GENERAL_ERROR',
    SOCKET_ERROR: 'SOCKET_ERROR',
    REDIS_ERROR: 'REDIS_ERROR',
  },

  RESPONSE_MESSAGE: {
    SUCCESS: 'OK',
    BAD_PARAMS: 'Bad Params',
    NOT_FOUND: 'Resource not Found',
    GENERAL_ERROR: 'Something went wrong',
    SOCKET_ERROR: 'Server error occured',
    REDIS_ERROR: 'Server error occured',
  }

}