const { RESPONSE_CODES } = require("./constants");

module.exports = {

  constructResponse(response, statusCode, errorMsg) {
    return response.status(statusCode).json({
      status: statusCode,
      message: errorMsg,
    });
  },

  constructSocketResponse(statusCode, errorMsg) {
    return {
      status: statusCode,
      message: errorMsg,
    }
  },

  // Testing response function
  constructResponseTest(responseType) {
    return {
      status: RESPONSE_CODES[responseType],
      data: responseType,
      // message:
    }
  },

}