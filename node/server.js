/**
 * Required External Modules
 */
const express = require("express");
const path = require("path");
const socket = require('socket.io');
const cors = require('cors');

const gameController = require('./controllers/game.controller');
const socketService = require('./services/sockets.service');
/**
 * App Variables
 */
const app = express();
const port = process.env.PORT || "8000";

/**
 *  App Configuration
 */
app.use(express.static('public'));
app.use(cors());


/**
 * Routes Definitions
 */

// app.get("/", (req, res) => {
//   res.status(200).send("game socket server.....");
// });


app.use('/game', gameController);

/**
 * Server Activation
 */

const server = app.listen(port, () => {
  console.log(`Listening to requests on http://localhost:${port}`);
});


socketService.initialiseSocket(server);