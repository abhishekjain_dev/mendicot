import anime from "animejs/lib/anime.es.js";

export default {
  data() {
    return {
    };
  },
  methods: {
    cardFlips(target) {
      var cardFlip = anime({
        targets: target,
        scale: [{ value: 1 }, { value: 1.4 }, { value: 1, delay: 250 }],
        rotateY: { value: "+=180", delay: 200 },
        easing: "easeInOutSine",
        duration: 400,
      });
    },
    getCord(winningPlayer){
      // Get the top, left coordinates of two elements
      let result = [];
      let winningPlayerEle = document.getElementById('animPlayer'+(winningPlayer +1))
      const targetRect = winningPlayerEle.getBoundingClientRect();
      
      for(let i = 1 ; i<=4; i++){
        let card = document.getElementsByClassName('player'+i);
        const cardRect = card[0].getBoundingClientRect();
      
        // Calculate the top and left positions
        
        const top = targetRect.top -  cardRect.top  
        const left = targetRect.left - cardRect.left;
        result[i-1] = {
          y: top + 'px',
          x: left + 'px'
        }
      }
      return result;
    },
    winningAnimation(playerindex){
      let allCord = this.getCord(playerindex);

      anime({
        targets: '.player1',
        translateX: {
          value: allCord[0].x,
          duration: 600,
        },
        translateY: {
          value: allCord[0].y,
          duration: 600,
        },
        easing: "easeInOutSine",
      });
      anime({
        targets: '.player2',
        translateX: {
          value: allCord[1].x,
          duration: 600,
        },
        translateY: {
          value: allCord[1].y,
          duration: 600,
        },
        easing: "easeInOutSine",
      });
            anime({
        targets: '.player3',
        translateX: {
          value: allCord[2].x,
          duration: 600,
        },
        translateY: {
          value: allCord[2].y,
          duration: 600,
        },
        easing: "easeInOutSine",
      });
            anime({
        targets: '.player4',
        translateX: {
          value: allCord[3].x,
          duration: 600,
        },
        translateY: {
          value: allCord[3].y,
          duration: 600,
        },
        easing: "easeInOutSine",
      });
    },
    distributeCards() {
      var self = this;
      var count = 0;
      var cardsDistributeAni = new Array()
      cardsDistributeAni[0] = anime({
        targets: ".card",
        translateX: {
          value: "-6vw",
          duration: 500,
        },
        translateY: {
          value: "-10vw",
          duration: 500,
        },
        autoplay: false,
        loop: true,
        easing: "easeInOutSine",
        loopBegin: function (anim) {
          if (count === 3) {
            anim.pause();
            count = 0;
            self.cardsDistributeAni[1].play();
          }
          count++;
          self.cardsDistributionComplete = false;
        },
      });
      this.cardsDistributeAni[1] = anime({
        targets: ".card",
        translateX: {
          value: "8vw",
          duration: 500,
        },
        translateY: {
          value: "-10vw",
          duration: 500,
        },
        autoplay: false,
        loop: true,
        easing: "easeInOutSine",
        loopBegin: function (anim) {
          if (count === 4) {
            anim.pause();
            count = 0;
            self.cardsDistributeAni[2].play();
          }
          count++;
          self.cardsDistributionComplete = false;
        },
      });
      this.cardsDistributeAni[2] = anime({
        targets: ".card",
        translateX: {
          value: "-8vw",
          duration: 500,
        },
        translateY: {
          value: "10vw",
          duration: 500,
        },
        autoplay: false,
        loop: true,
        easing: "easeInOutSine",
        loopBegin: function (anim) {
          if (count === 4) {
            anim.pause();
            count = 0;
            self.cardsDistributeAni[3].play();
          }
          count++;
          self.cardsDistributionComplete = false;
        },
      });
      this.cardsDistributeAni[3] = anime({
        targets: ".card",
        translateX: {
          value: "6vw",
          duration: 500,
        },
        translateY: {
          value: "10vw",
          duration: 500,
        },
        autoplay: false,
        loop: true,
        easing: "easeInOutSine",
        loopBegin: function (anim) {
          if (count === 4) {
            anim.pause();
            count = 0;
            self.cardsDistributionComplete = true;
          } else {
            count++;
            self.cardsDistributionComplete = false;
          }
        },
      });
      cardsDistributeAni[0].play()
    },
    cardHover(el, start) {
      anime.remove(el);
      if(start){
        anime({
          targets: el,
          scale: 1.2,
          duration: 800,
          elasticity: 400
        });
      }
      else{
        anime({
          targets: el,
          scale: 1.0,
          duration: 600,
          elasticity: 300
        });
      }

    }
  }
};