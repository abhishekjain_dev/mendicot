export default {
  data() {
    return {

    }
  },
  methods: {
    // min and max included
    randomIntFromInterval(min, max) {
      return Math.floor(Math.random() * (max - min + 1) + min).toString();
    },
    reverseString(str) {
      var splitString = str.split("");
      var reverseArray = splitString.reverse(); 
      var joinArray = reverseArray.join(""); 
      return joinArray;
}
  }
}