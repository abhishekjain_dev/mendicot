export default {
  data() {
    return {
      options: {
        labels: {
          buttonSkip: 'Skip tour',
          buttonPrevious: 'Previous',
          buttonNext: 'Next',
          buttonStop: 'Finish'
        }
      },
      gameSteps:[
        {
          target: '#v-game-step-0',  // We're using document.querySelector() under the hood
          header: {
            title: 'Trump',
          },
          content: `When you want to open trump, click here`
        },
        {
          target: '#v-game-step-1',  // We're using document.querySelector() under the hood
          header: {
            title: 'Cards In Hand',
          },
          content: `When its your turn, drag and drop the card on the table to play it`
        },
        {
          target: '#v-game-step-2',  // We're using document.querySelector() under the hood
          header: {
            title: 'Play Time',
          },
          content: `Time remaining for the player to play their card can be seen by the increasing avatar border color`
        },
      ],
      changeTeamSteps: [
        {
          target: '#v-change-team-step-0',  // We're using document.querySelector() under the hood
          header:{
            title: 'Change Team',
          },
          content: `To adjust team members, drag the player to a different team`
        },
      ],
      changeTeamCallback: {
        onFinish: this.changeTeamFinishTour
      },
      gameCallback: {
        onFinish: this.gameFinishTour
      },
    }
  },
  methods: {

    // all callbacks
    changeTeamFinishTour() {
      this.$q.localStorage.set("changeTeamTour", true);
    },
    gameFinishTour() {
      this.$q.localStorage.set("gameTour", true);
    },

    // init tours
    m_showTrumpTour(){
      if (!this.$q.localStorage.getItem("gameTour")) {
        this.$tours['gameTour'].start()
      }
    },
    m_showChangeTeamTour() {
      if (!this.$q.localStorage.getItem("changeTeamTour")) {
        this.$tours['myTour'].start()
      }
    },

  },
}