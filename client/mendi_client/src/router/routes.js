
const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', component: () => import('pages/intro.vue') },
      { path: 'session', name: "game", component: () => import('pages/sessionPage.vue') },
      { path: 'rules', name: "rules", component: () => import('pages/rules.vue') },
      { path: 'privacyPolicy', component: () => import('pages/privacyPolicy.vue') }
    ]
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '*',
    component: () => import('pages/Error404.vue')
  }
]

export default routes
