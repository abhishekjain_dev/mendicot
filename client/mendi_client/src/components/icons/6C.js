/* eslint-disable */
var icon = require('vue-svgicon')
icon.register({
  '6C': {
    width: 240,
    height: 336,
    viewBox: '-120 -168 240 336',
    data: '<symbol id="svgicon_6C_b" viewBox="-500 -500 1000 1000" preserveAspectRatio="xMinYMid"><path pid="0" d="M-250 100a250 250 0 01500 0v110a250 250 0 01-500 0v-420A250 250 0 010-460c150 0 180 60 200 85" _stroke="#000" stroke-width="80" stroke-linecap="square" stroke-miterlimit="1.5" _fill="none"/></symbol><symbol id="svgicon_6C_a" viewBox="-600 -600 1200 1200" preserveAspectRatio="xMinYMid"><path pid="1" d="M30 150c5 235 55 250 100 350h-260c45-100 95-115 100-350a10 10 0 00-20 0 210 210 0 11-74-201 10 10 0 0014-14 230 230 0 11220 0 10 10 0 0014 14 210 210 0 11-74 201 10 10 0 00-20 0z"/></symbol><rect pid="2" width="239" height="335" x="-119.5" y="-167.5" rx="12" ry="12" _fill="#fff" _stroke="#000"/><use xlink:href="#svgicon_6C_a" height="70" width="70" x="-87.501" y="-35"/><use xlink:href="#svgicon_6C_a" height="70" width="70" x="17.501" y="-35"/><use xlink:href="#svgicon_6C_b" height="32" width="32" x="-114.4" y="-156"/><use xlink:href="#svgicon_6C_a" height="26.769" width="26.769" x="-111.784" y="-119"/><use xlink:href="#svgicon_6C_a" height="70" width="70" x="-87.501" y="-135.588"/><use xlink:href="#svgicon_6C_a" height="70" width="70" x="17.501" y="-135.588"/><g transform="rotate(180)"><use xlink:href="#svgicon_6C_b" height="32" width="32" x="-114.4" y="-156"/><use xlink:href="#svgicon_6C_a" height="26.769" width="26.769" x="-111.784" y="-119"/><use xlink:href="#svgicon_6C_a" height="70" width="70" x="-87.501" y="-135.588"/><use xlink:href="#svgicon_6C_a" height="70" width="70" x="17.501" y="-135.588"/></g>'
  }
})
