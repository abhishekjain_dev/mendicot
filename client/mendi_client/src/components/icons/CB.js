/* eslint-disable */
var icon = require('vue-svgicon')
icon.register({
  'CB': {
    width: 208,
    height: 303,
    viewBox: '0 0 208 303',
    data: '<clipPath id="svgicon_CB_a"><rect pid="0" x=".5" y=".5" width="207" height="302" rx="8"/></clipPath><g clip-path="url(#svgicon_CB_a)"><path pid="1" _fill="#FFF" d="M0 0h208v303H0"/><path pid="2" _stroke="#D11209" stroke-width="430" stroke-dasharray="3.67" d="M0 294L306-9"/></g><rect pid="3" _stroke="#000" stroke-width=".5" x=".5" y=".5" width="207" height="302" rx="8" _fill="none"/>'
  }
})
