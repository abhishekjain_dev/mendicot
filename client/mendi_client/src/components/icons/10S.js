/* eslint-disable */
var icon = require('vue-svgicon')
icon.register({
  '10S': {
    width: 240,
    height: 336,
    viewBox: '-120 -168 240 336',
    data: '<symbol id="svgicon_10S_b" viewBox="-600 -600 1200 1200" preserveAspectRatio="xMinYMid"><path pid="0" d="M0-500c100 250 355 400 355 685a150 150 0 01-300 0 10 10 0 00-20 0c0 200 50 215 95 315h-260c45-100 95-115 95-315a10 10 0 00-20 0 150 150 0 01-300 0c0-285 255-435 355-685z"/></symbol><symbol id="svgicon_10S_a" viewBox="-500 -500 1000 1000" preserveAspectRatio="xMinYMid"><path pid="1" d="M-260 430v-860M-50 0v-310a150 150 0 01300 0v620a150 150 0 01-300 0z" _stroke="#000" stroke-width="80" stroke-linecap="square" stroke-miterlimit="1.5" _fill="none"/></symbol><rect pid="2" width="239" height="335" x="-119.5" y="-167.5" rx="12" ry="12" _fill="#fff" _stroke="#000"/><use xlink:href="#svgicon_10S_a" height="32" width="32" x="-114.4" y="-156"/><use xlink:href="#svgicon_10S_b" height="26.769" width="26.769" x="-111.784" y="-119"/><use xlink:href="#svgicon_10S_b" height="70" width="70" x="-87.501" y="-135.501"/><use xlink:href="#svgicon_10S_b" height="70" width="70" x="17.501" y="-135.501"/><use xlink:href="#svgicon_10S_b" height="70" width="70" x="-87.501" y="-68.5"/><use xlink:href="#svgicon_10S_b" height="70" width="70" x="17.501" y="-68.5"/><use xlink:href="#svgicon_10S_b" height="70" width="70" x="-35" y="-102"/><g transform="rotate(180)"><use xlink:href="#svgicon_10S_a" height="32" width="32" x="-114.4" y="-156"/><use xlink:href="#svgicon_10S_b" height="26.769" width="26.769" x="-111.784" y="-119"/><use xlink:href="#svgicon_10S_b" height="70" width="70" x="-87.501" y="-135.501"/><use xlink:href="#svgicon_10S_b" height="70" width="70" x="17.501" y="-135.501"/><use xlink:href="#svgicon_10S_b" height="70" width="70" x="-87.501" y="-68.5"/><use xlink:href="#svgicon_10S_b" height="70" width="70" x="17.501" y="-68.5"/><use xlink:href="#svgicon_10S_b" height="70" width="70" x="-35" y="-102"/></g>'
  }
})
