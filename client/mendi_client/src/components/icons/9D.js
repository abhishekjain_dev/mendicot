/* eslint-disable */
var icon = require('vue-svgicon')
icon.register({
  '9D': {
    width: 240,
    height: 336,
    viewBox: '-120 -168 240 336',
    data: '<symbol id="svgicon_9D_b" viewBox="-500 -500 1000 1000" preserveAspectRatio="xMinYMid"><path pid="0" d="M250-100a250 250 0 01-500 0v-110a250 250 0 01500 0v420A250 250 0 010 460c-150 0-180-60-200-85" _stroke="red" stroke-width="80" stroke-linecap="square" stroke-miterlimit="1.5" _fill="none"/></symbol><symbol id="svgicon_9D_a" viewBox="-600 -600 1200 1200" preserveAspectRatio="xMinYMid"><path pid="1" d="M-400 0C-350 0 0-450 0-500 0-450 350 0 400 0 350 0 0 450 0 500 0 450-350 0-400 0z" _fill="red"/></symbol><rect pid="2" width="239" height="335" x="-119.5" y="-167.5" rx="12" ry="12" _fill="#fff" _stroke="#000"/><use xlink:href="#svgicon_9D_a" height="70" width="70" x="-35" y="-35"/><use xlink:href="#svgicon_9D_b" height="32" width="32" x="-114.4" y="-156"/><use xlink:href="#svgicon_9D_a" height="26.769" width="26.769" x="-111.784" y="-119"/><use xlink:href="#svgicon_9D_a" height="70" width="70" x="-87.501" y="-135.501"/><use xlink:href="#svgicon_9D_a" height="70" width="70" x="17.501" y="-135.501"/><use xlink:href="#svgicon_9D_a" height="70" width="70" x="-87.501" y="-68.5"/><use xlink:href="#svgicon_9D_a" height="70" width="70" x="17.501" y="-68.5"/><g transform="rotate(180)"><use xlink:href="#svgicon_9D_b" height="32" width="32" x="-114.4" y="-156"/><use xlink:href="#svgicon_9D_a" height="26.769" width="26.769" x="-111.784" y="-119"/><use xlink:href="#svgicon_9D_a" height="70" width="70" x="-87.501" y="-135.501"/><use xlink:href="#svgicon_9D_a" height="70" width="70" x="17.501" y="-135.501"/><use xlink:href="#svgicon_9D_a" height="70" width="70" x="-87.501" y="-68.5"/><use xlink:href="#svgicon_9D_a" height="70" width="70" x="17.501" y="-68.5"/></g>'
  }
})
