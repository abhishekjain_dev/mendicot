/* eslint-disable */
var icon = require('vue-svgicon')
icon.register({
  '3H': {
    width: 240,
    height: 336,
    viewBox: '-120 -168 240 336',
    data: '<symbol id="svgicon_3H_b" viewBox="-500 -500 1000 1000" preserveAspectRatio="xMinYMid"><path pid="0" d="M-250-320v-140h450L-110-80c10-10 60-40 110-40 200 0 250 120 250 270 0 200-80 310-280 310s-230-160-230-160" _stroke="red" stroke-width="80" stroke-linecap="square" stroke-miterlimit="1.5" _fill="none"/></symbol><symbol id="svgicon_3H_a" viewBox="-600 -600 1200 1200" preserveAspectRatio="xMinYMid"><path pid="1" d="M0-300c0-100 100-200 200-200s200 100 200 250C400 0 0 400 0 500 0 400-400 0-400-250c0-150 100-250 200-250S0-400 0-300z" _fill="red"/></symbol><rect pid="2" width="239" height="335" x="-119.5" y="-167.5" rx="12" ry="12" _fill="#fff" _stroke="#000"/><use xlink:href="#svgicon_3H_a" height="70" width="70" x="-35" y="-35"/><use xlink:href="#svgicon_3H_b" height="32" width="32" x="-114.4" y="-156"/><use xlink:href="#svgicon_3H_a" height="26.769" width="26.769" x="-111.784" y="-119"/><use xlink:href="#svgicon_3H_a" height="70" width="70" x="-35" y="-135.501"/><g transform="rotate(180)"><use xlink:href="#svgicon_3H_b" height="32" width="32" x="-114.4" y="-156"/><use xlink:href="#svgicon_3H_a" height="26.769" width="26.769" x="-111.784" y="-119"/><use xlink:href="#svgicon_3H_a" height="70" width="70" x="-35" y="-135.501"/></g>'
  }
})
