import Vue from 'vue'
import Vuex from 'vuex'
import _ from 'lodash'
import VuexPersistence from 'vuex-persist'

// import example from './module-example'

Vue.use(Vuex)

const vuexLocal = new VuexPersistence({
  storage: window.sessionStorage
})

/*
 * If not building with SSR mode, you can
 * directly export the Store instantiation;
 *
 * The function below can be async too; either use
 * async/await or return a Promise which resolves
 * with the Store instance.
 */

export default function (/* { ssrContext } */) {
  const Store = new Vuex.Store({
    state: {
      GAME_URL: process.env.DEV ? 'http://localhost:8080/#/' :"https://playmendi.com/#/",

      users: {},
      trumpOpened: false,
      trumpType: '',
      trumpHolder: '',
      trumpCard: {},
      gameId: '',
      playerId: '',
      playerCards: [],
      responsiveSizes: {
        mobile: {
          card: '3.5rem',
          table: 'height: 10rem',
          avatar: '2.5rem',
          avatarProgress: '3rem',
          avatarLoader: '4rem',
          scoreboard: ''
        },
        desktopMd: {
          card: '5rem',
          table: 'height: 17rem',
          avatar: '3rem',
          avatarProgress: '60px',
          avatarLoader: '5rem',
          scoreboard: ''
        },
        desktopLg: {
          card: '5.5rem',
          table: 'height: 18rem',
          avatar: '3rem',
          avatarProgress: '60px',
          avatarLoader: '6rem',
          scoreboard: ''
        },
        desktopXl: {
          card: '8rem',
          table: 'height: 18rem',
          avatar: '4rem',
          avatarProgress: '60px',
          avatarLoader: '6rem',
          scoreboard: ''
        }

      },
      isTrumpOpenedByCurrentPlayer: false,
      turns:[],
      playerCount: 0,
      roundDetails:{
        roundNo: 0,
        currentTurn: '',
        nextTrun: '',
        cardsPlayed: []
      },
      scoreBoard: {
        gameNo: 1,
        red: {
          roundsWon: 0,
          mendi: []
        },
        blue: {
          roundsWon: 0,
          mendi: []
        }
      },
      gameOver: false,
    },
    getters: {
      getPlayerCards: state => {
        // state.playerCards = _.sortBy(state.playerCards,['type','rank'])
        return state.playerCards;
      },
      getPlatformDetails: (state) => (screen) => {
        if (screen.md){
          return state.responsiveSizes.desktopMd;
        }
        else if (screen.lg) {
          return state.responsiveSizes.desktopLg;
        }
        else if (screen.xs || screen.sm) {
          return state.responsiveSizes.mobile;
        }
        else if (screen.xl){
          return state.responsiveSizes.desktopXl;
        }
      },
      getPlayerInfo: (state) => {
        return state.users[state.playerId];
      },
      getTrumpType: (state) => {
        return state.trumpType;
      },
      getGameId: (state) => {
        return state.gameId;
      },
      getScoreboard: (state) =>{
        return state.scoreBoard
      },
      getUsers: (state) =>{
        return state.users;
      },
      getPlayableCard: (state) =>{
        let roundCards = state.roundDetails.cardsPlayed
        // if no other card is played then return any card that the player has which is not played before
        if(roundCards.length == 0){
          for (let card of state.playerCards){
            if(!card.isPlayed){
              return card;
            }
          }  
        }
        else{
          let firstCard = roundCards[0];
          for (let card of state.playerCards) {
            if (!card.isPlayed && card.type == firstCard.card.type) {
              return card;
            }
          }
          // if the player does not have the type of first card played then return any card
          for (let card of state.playerCards) {
            if (!card.isPlayed) {
              return card;
            }
          }  
        }
        return null;
        
      }
    },
    mutations: {
      resetState(state, isNewGame = true){
        if(!isNewGame){
          state.users = {};
          state.playerId = ''
          state.gameId = ''
        } 
        state.scoreBoard = {
          gameNo: 1,
          red: {
            roundsWon: 0,
            mendi: []
          },
          blue: {
            roundsWon: 0,
            mendi: []
          }
        },
        state.gameOver = false;
        state.turns = [];
        state.isTrumpOpenedByCurrentPlayer = false;
        state.playerCards = []
        state.trumpOpened = false;
        if (isNewGame) {
          if (state.trumpHolder) {
            let trumpHolder = state.trumpHolder;
            state.users[trumpHolder]['hasTrump'] = false;
          }
        }
        state.trumpCard = {};
        state.trumpType = '';
        state.trumpHolder = '';
        state.roundDetails = {}
      },
      gameOver(state){
        state.gameOver = true;
      },
      changeTrumpOpenedByMe(state, isTrumpOpenedByPlayer){
        state.isTrumpOpenedByCurrentPlayer = isTrumpOpenedByPlayer;
      },
      setPlayerInfo(state, info) {
        // state.playerInfo = info;
        const id = info.id;
        state.playerId = id;
        state.users[id] = info;
      },
      addNewPlayer(state, playerInfo){
        const id = playerInfo.id;
        state.users[id] = playerInfo;
      },
      changePlayerTeam(state, data){
        state.users[data.id].team = data.newTeam
      },
      setGameId(state, gameId) {
        state.gameId = gameId;
      },
      initRound(state, roundDetails){
        this.commit('resetState');
        state.playerCount = roundDetails.playercount;
        state.turns = JSON.parse(JSON.stringify(roundDetails.round.turns));
        if (roundDetails.isMobile){
          this.commit('reOrderTurnsForMobile');
        }
        // delete roundDetails.round.turns;
        state.roundDetails = roundDetails.round
        state.playerCards = roundDetails.playerData.cardsInHand
        // format cards in hand        
        let trumpHolder = roundDetails.commonData.trumpHolder;
        state.trumpHolder = trumpHolder;
        state.users[trumpHolder]['hasTrump'] = true;
        state.gameOver = false;
        state.isTrumpOpenedByCurrentPlayer = false;
        state.trumpOpened = false;

      },
      updateScoreBoard(state, data){
        state.scoreBoard = data;

        // reset cardsPlayed
        state.roundDetails.cardsPlayed = [];
      },
      updateRound(state, round){
        state.roundDetails = round;
        let cardsPlayed = state.roundDetails.cardsPlayed;

        // if player has played a card change isPlayed value of that card in playerCards;
        cardsPlayed.forEach(cardDetails => {
          if(cardDetails.player == state.playerId){
            state.playerCards.every(card =>{
              if (card.imgName == cardDetails.card.imgName){
                card.isPlayed = true;
                return false;
              }
              return true;
            })
          }
        });
      },

      addTrumpToPlayerhand(state, data){
        // "trumpHolder": "socketId_3",
        //   "trumpCard": {
        //   "imgName": "QC",
        //     "isPlayed": false,
        //       "type": "Clubs",
        //         "rank": 12
        // }
        if (state.playerId == data.trumpHolder){
          state.playerCards.push(data.trumpCard);
        }
      },
      trumpOpened(state, trumpCard){
        state.trumpCard = trumpCard;
        state.trumpOpened = true;
        state.trumpType = trumpCard.type;
      },
      reOrderTurnsForMobile(state){
        function rotateRight(arr) {
          let last = arr.pop();
          arr.unshift(last);
          return arr;
        }
        let playerIndex = _.indexOf(state.turns, state.playerId);
        if(playerIndex != 2){
          let mathIndex = playerIndex + 1;
          let timesToRotate = mathIndex != 4 ? 3 - mathIndex : 3;
          for (let index = 0; index < timesToRotate; index++) {
            rotateRight(state.turns)
          }
        }
      },
      removeUser(state, userId){
        delete state.users[userId];
      }
      

    },
    actions: {
      resetState({ commit, state }, isNewGame) {
        commit('resetState', isNewGame);
      },
      gameOver({ commit, state}) {
        commit('gameOver');
      },
      changeTrumpOpenedByMe({ commit, state }, isTrumpOpenedByPlayer) {
        commit('changeTrumpOpenedByMe', isTrumpOpenedByPlayer);
      },
      setPlayerInfo({ commit }, info) {
        commit('setPlayerInfo', info);
      },
      addNewPlayer({ commit }, playerInfo) {
        commit('addNewPlayer', playerInfo);
      },
      changePlayerTeam({ commit }, data) {
        commit('changePlayerTeam', data);
      },
      setGameId({ commit }, gameId) {
        commit('setGameId', gameId);
      },
      initRound({ commit }, roundDetails, isMobile) {
        commit('initRound', roundDetails, isMobile);
      },
      updateScoreBoard({ commit }, data) {
        commit('updateScoreBoard', data);
      },
      updateRound({ commit }, round) {
        commmit('updateRound', round);
      },
      addTrumpToPlayerhand({ commit }, data) {
        commit('addTrumpToPlayerhand', data);
      },
      trumpOpened({ commit }, trumpCard) {
        commit('trumpOpened', trumpCard);
      },
      reOrderTruns({ commit }){
        commit('reOrderTurnsForMobile')
      },
      removeUser({ commit },playerId){
        commit('removeUser',playerId)
      }
    },
    modules: {
      // example
    },
    

    plugins: [vuexLocal.plugin],

    // enable strict mode (adds overhead!)
    // for dev mode only
    strict: process.env.DEBUGGING
  })

  return Store
}
