import Vue from 'vue'
import VueBus from 'vue-bus';


export default async ({ app, store, Vue }) => {
  Vue.use(VueBus);
}