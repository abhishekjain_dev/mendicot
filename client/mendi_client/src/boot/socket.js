import { io } from 'socket.io-client';
import VueSocketIOExt from 'vue-socket.io-extended';
var url = '';
if (process.env.DEV){
  url = 'http://localhost:8000';
}
else{
  url = 'https://mendinode.onrender.com'
}

const socket = io(url, {
  autoConnect: false
});

export default async ({ store, Vue }) => {
  Vue.use(VueSocketIOExt, socket, { store, actionPrefix: 'SOCKET_', })
}