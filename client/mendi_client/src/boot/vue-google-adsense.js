import Vue from 'vue'
import Ads from 'vue-google-adsense'



export default async ({app, store, Vue }) => {
  Vue.use(require('vue-script2'))

  Vue.use(Ads.Adsense)
  Vue.use(Ads.InArticleAdsense)
  Vue.use(Ads.InFeedAdsense)
}